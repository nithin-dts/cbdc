#!/usr/bin/python3.9

import requests
import json
import getpass
import sys
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# Function to log in and get the session token
def logon(username, password):
    url = "https://10.198.51.2/PasswordVault/api/auth/ldap/logon"
    payload = {"username": username, "password": password, "concurrentSession": True}
    response = requests.post(url, json=payload, verify=False)
    response.raise_for_status()
    value = response.text[1:-1]
    return value

# Function to get account details
def get_accounts(session_token):
    url = "https://10.198.51.2/PasswordVault/api/accounts"
    headers = {"Authorization": f"{session_token}"}
    response = requests.get(url, headers=headers, verify=False)
    response.raise_for_status()
    return response.json()

# Function to retrieve the secret
def retrieve_secret(session_token, account_id, target):
    url = f"https://10.198.51.2/PasswordVault/API/Accounts/{account_id}/Secret/Retrieve"
    payload = {"reason": "remote connection", "ActionType": "string", "Machine": f"{target}"}
    headers = {"Authorization": f"{session_token}"}
    response = requests.post(url, json=payload, headers=headers, verify=False)
    value = response.text
    return value

# Function to log off
def logoff(session_token):
    url = "https://10.198.51.2/PasswordVault/API/Auth/Logoff/"
    headers = {"Authorization": f"{session_token}"}
    response = requests.post(url, headers=headers, verify=False)
    response.raise_for_status()
    return response.json()

def main():
    # Prompt for password input
    username = sys.argv[2]
    password = sys.argv[3]

    # Log in to get the session token
    session_token = logon(username, password)

    # Get the address argument from the command line
    if len(sys.argv) < 4:
        print("Please provide the address, username and password as a command line argument.")
        sys.exit(1)

    address = sys.argv[1]

    # Get accounts
    accounts_output = get_accounts(session_token)
    a = json.dumps(accounts_output['value'])
    accounts = json.loads(a)

    for a in accounts:
        if (a['address'] == address):
            account = a

    # Find the account matching the provided address
    #account = next((acc for acc in accounts['value'] if acc['address'] == address), None)

    if not account:
        print(f"No account found for address: {address}")
        sys.exit(1)

    account_id = account['id']
    account_username = account['userName']

    # Retrieve the secret (password)
    password = retrieve_secret(session_token, account_id, address)

    # Print the username and password in JSON format
    result = {
        "username": account_username,
        "password": password
    }
    print(json.dumps(result, indent=4))

    # Log off
    #logoff(session_token)

if __name__ == "__main__":
    main()
